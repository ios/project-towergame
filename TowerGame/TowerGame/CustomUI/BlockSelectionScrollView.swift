//
//  BlockSelectionScrollView.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import UIKit

/// Subclass of UIScrollView with animations for making the ScrollView fade in and out.
class BlockSelectionScrollView: UIScrollView {
    
    var fadeTimer: Timer = Timer()
    var timeInterval = 2.0
    
    /// Makes the BlockSelectionScrollView fade in and fully visible.
    @objc func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
            UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.alpha = 1.0
            }, completion: completion)
        }
    
    /// Makes the BlockSelectionScrollView fade out and transparent.
    @objc func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.3
        }, completion: completion)
    }
}
