//
//  RoundedButton.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import UIKit

/// Subclass of UIButton, that has rounded corners, if set so.
@IBDesignable class RoundedButton: UIButton {
    
    /// Override the layoutSubviews function to update the cornerRadius.
    override func layoutSubviews() {
        super.layoutSubviews()

        updateCornerRadius()
    }

    /// Is true, if the corners of the button should be rounded
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    /// Updates the cornerRadius to make the left and right side of the button completely round.
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
}
