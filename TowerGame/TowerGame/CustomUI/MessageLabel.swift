//
//  MessageLabel.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import UIKit

/// Subclass of UILabel, that allows to show short messages to the user.
class MessageLabel: UILabel {
    
    /// Override the intrinsicContentSize to also include the size of the inset that's added when text is drawn.
    override var intrinsicContentSize: CGSize {
        get {
            var size = super.intrinsicContentSize
            size.height += paddingTop + paddingBottom
            size.width += paddingLeft + paddingRight
            return size
        }
    }
    
    /// Padding at the top of the label.
    @IBInspectable var paddingTop: CGFloat = 5
    
    /// Padding at the bottom of the label.
    @IBInspectable var paddingBottom: CGFloat = 5
    
    /// Padding to the left oft the label
    @IBInspectable var paddingLeft: CGFloat = 5
    
    /// Padding to the right oft the label
    @IBInspectable var paddingRight: CGFloat = 5
    
    /// Override the UILabels drawText-funtion to include padding around the label.
    override func drawText(in rect: CGRect) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5
        
        let insets = UIEdgeInsets(top: self.paddingTop, left: self.paddingLeft, bottom: self.paddingBottom, right: self.paddingRight)
        
        super.drawText(in: rect.inset(by: insets))
    }
    
    /// Function to make the label visible to the user for the given duration with the given text.
    func displayMessage(_ text: String, duration: TimeInterval = 3.0) {
        DispatchQueue.main.async {
            self.text = text
            
            if self.isHidden {
                self.show()
            }
            
            // Increase tag to indicate update of the Label
            let tag = self.tag + 1
            self.tag = tag
            
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                // Do not hide the label, if it has been updated
                if self.tag == tag {
                    self.hide()
                }
            }
        }
    }
    
    /// Function to show the label with an animation.
    private func show() {
        UIView.transition(with: self,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.isHidden = false
                          }
        )
    }
    
    /// Function to hide the label with an animation.
    private func hide() {
        UIView.transition(with: self,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.isHidden = true
                          }
        )
    }
}
