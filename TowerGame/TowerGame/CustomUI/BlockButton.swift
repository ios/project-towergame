//
//  BlockButton.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import UIKit

/// Normal UIButton, that has additional information about the block it is used for
final class BlockButton: UIButton {
    
    /// Stores for which block this button is and automatically loads it's image, if it exists. Otherwise it will set the title of the block, to the block name.
    var forBlock: String? {
        didSet{
            if let image = UIImage(named: forBlock!) {
                self.setImage(image, for: .normal)
                self.imageView!.contentMode = UIView.ContentMode.scaleAspectFit
            } else {
                self.setTitle(forBlock, for: .normal)
            }
        }
    }
    
}
