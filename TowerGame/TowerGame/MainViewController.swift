//
//  MainViewController.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import UIKit

class MainViewController: UIViewController {
    let soundController = SoundController()
    
    /// Determine if the game is started in Multiplayer or Singleplayer and play a sound.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let gameVC = segue.destination as! GameViewController
        gameVC.isMultiplayer = segue.identifier == "multiPlayerSegue"
        soundController.play(sound: .warmTone)
    }
}
