//
//  GameViewController+ARCoachingOverlay.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import ARKit

extension GameViewController: ARCoachingOverlayViewDelegate {

    /// Create and setup the ARCoachingOverlay. Defines the goal to find a horizontal plane and adds the CoachingOverlay as subview to the ARView.
    func setupCoachingOverlay() {
        // Create a ARCoachingOverlayView object
        let coachingOverlay = ARCoachingOverlayView()
        // Set up constraints so that the coaching overlay appears in the center of the screen
        arView.addSubview(coachingOverlay)
        coachingOverlay.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([NSLayoutConstraint(item: coachingOverlay,
                                                        attribute: .top, relatedBy: .equal,
                                                        toItem: self.view, attribute: .top,
                                                        multiplier: 1, constant: 0),
                                     NSLayoutConstraint(item:  coachingOverlay,
                                                        attribute: .bottom, relatedBy: .equal,
                                                        toItem: self.view, attribute: .bottom,
                                                        multiplier: 1, constant: 0),
                                     NSLayoutConstraint(item:  coachingOverlay,
                                                        attribute: .leading, relatedBy: .equal,
                                                        toItem: self.view, attribute: .leading,
                                                        multiplier: 1, constant: 0),
                                     NSLayoutConstraint(item:  coachingOverlay,
                                                        attribute: .trailing, relatedBy: .equal,
                                                        toItem: self.view, attribute: .trailing,
                                                        multiplier: 1, constant: 0)])
        
        // Set the Augmented Reality goal
        coachingOverlay.goal = .horizontalPlane
        // Set the ARSession
        coachingOverlay.session = arView.session
        // Set the delegate for any callbacks
        coachingOverlay.delegate = self
        coachingOverlay.activatesAutomatically = true
    }
    
    /// Implementation of the delegate function of the ARCoachingOverlay that gets called when the CoachingOverlay will disappear. This happens when the goal is reached, so the boolean finishedWorldRecognition is set to true.
    func coachingOverlayViewDidDeactivate(_ coachingOverlayView: ARCoachingOverlayView) {
        self.finishedWorldRecognition = true
    }
}
