//
//  GameViewController+FollowingBlock.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import RealityKit
import Combine

/// Block that follows the player.
struct FollowingBlock {
    /// Name of the block.
    let name: String
    
    /// Rotation around the z-axis (from the users point of view) in degrees.
    var rotation = 0
    
    /// Distance of the block to the player.
    var distance: Float = 0.4
    
    /// ModelEntity that gets displayed below the block to show where it will fall.
    let projection: ModelEntity
    
    /// Handler that makes the block follow the camera.
    let followHandler: Cancellable
    
    /// ModelEntity of the block that should follow the camera.
    let model: ModelEntity
    
    /// AnchorEntity, that the block is attached to. Will be moved to follow the camera.
    var anchor: AnchorEntity
}

extension GameViewController {
    
    /// Creates a FollowingBlock that will follow the device of the player until it is either placed or replaced with another block.
    func createFollowingBlock(named blockName: String) {
        // If there already was a block following the camera, remove it.
        removeFollowingBlock()
        
        // subscribe to updates for changing position on camera movement until cancelled
        let followHandler = arView.scene.subscribe(to: SceneEvents.Update.self) { [self] (event) in
            guard let followingBlock = self.followingBlock else { return }
            
            // Set the position of the FollowingBlock.
            followingBlock.anchor.transform = moveAway(from: arView.cameraTransform, distance: followingBlock.distance, rotateBy: followingBlock.rotation)
            
            // initially set the default block color
            followingBlock.model.model?.materials = [SimpleMaterial(color: .blue, isMetallic: false)]
            
            // check if the block overlaps with any other block in the scene
            if checkOverlap(followingBlock.model) {
                // set material color to red if overlapping
                followingBlock.model.model?.materials = [SimpleMaterial(color: .red, isMetallic: false)]
            }
            
            adjustProjection()
        }
        
        // Create new FollowingBlock
        self.followingBlock = FollowingBlock(name: blockName,
                                             projection: generateProjection(),
                                             followHandler: followHandler,
                                             model: getModelEntity(named: blockName),
                                             anchor: AnchorEntity(world: [0,0,0]))
        
        
        // add a new anchor entity
        arView.scene.addAnchor(self.followingBlock!.anchor)
        
        // add the model of the block and it's projection to the anchor entity
        followingBlock?.anchor.addChild(followingBlock!.model)
        followingBlock?.anchor.addChild(followingBlock!.projection)
        
        gameState = .placingBlock
    }
    
    /// Sets the FollowingBlock to nil and removes all visual components of it from the scene.
    func removeFollowingBlock() {
        if let followingBlock = self.followingBlock {
            followingBlock.followHandler.cancel()
            followingBlock.projection.removeFromParent()
            followingBlock.model.removeFromParent()
            
            self.followingBlock = nil
        }
    }
}
