//
//  BlockSelectionScrollView.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//
import UIKit

extension GameViewController: UIScrollViewDelegate {
    
    /// Create the BlockSelectionView, that has a button for each block included in the game.
    func setupBlockSelectionView() {
        var frame : CGRect?
        let buttonWidth : CGFloat = 115
        let buttonMargin : CGFloat = 0
        for i in 0..<meshes!.count {
            // Create a BlockButton for each block
            let button = BlockButton(type: .custom)
            let buttonXPosition : CGFloat = buttonMargin + (CGFloat(i) * (buttonWidth + buttonMargin))
            frame = CGRect(x: buttonXPosition, y: 10, width: buttonWidth, height: 80)
            
            // Make the background of the button transparent.
            button.backgroundColor = UIColor(red: 0, green: 0, blue:0, alpha: 0.0)
            button.frame = frame!
            
            // Set for which block this button is. This also sets an image for the button.
            button.forBlock = meshes![i].name
            
            // Add a listener to the button that lets the user select the given block.
            button.addTarget(self, action: #selector(selectBlock), for: .touchUpInside)
            blockSelectionScrollView.addSubview(button)
        }
        blockSelectionScrollView.contentSize = CGSize(width: CGFloat(meshes!.count) * (buttonWidth + buttonMargin) + buttonMargin, height: blockSelectionScrollView.frame.size.height)
        
        // Set self as delegate for the blockSelectionScrollView.
        blockSelectionScrollView.delegate = self
    }
    
    /// Select a block and let it appear in front of the player.
    @objc func selectBlock(sender: AnyObject) {
        guard let button = sender as? BlockButton, let blockName = button.forBlock else {
            return
        }
        // Prints selected block and change the type of block to place
        print("Select block \(blockName)")
        
        self.gameState = .placingBlock
        
        // Places block in front of camera
        createFollowingBlock(named: blockName)
        
        // fade in block selection menu
        fadeInBlockSelection()
        
        // fade in block rotation buttons
        fadeInRotationButtons()
    }
    
    /// Starts timer and calls fadeOutBlockSelection() after timer reaches the end
    func startTimer() {
        self.blockSelectionScrollView.fadeTimer = Timer.scheduledTimer(timeInterval: self.blockSelectionScrollView.timeInterval, target: self, selector: (#selector(fadeOutBlockSelection)), userInfo: nil, repeats: true)
    }
    
    /// Fades out the blockSelection, if the user has a block selected currently.
    @objc func fadeOutBlockSelection() {
        if self.gameState == .placingBlock {
            self.blockSelectionScrollView.fadeOut()
        }
    }
    
    /// Fades in the block selection and starts timer to fade out
    @objc func fadeInBlockSelection() {
        self.blockSelectionScrollView.fadeIn()
        self.blockSelectionScrollView.fadeTimer.invalidate()
        startTimer()
    }
    
    /// Implementation of the UIScrollView's delegate function that lets the ScrollView fade in, if the user scrolled it.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        fadeInBlockSelection()
    }
}
