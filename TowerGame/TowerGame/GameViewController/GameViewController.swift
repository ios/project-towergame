//
//  ViewController.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import UIKit
import RealityKit
import ARKit
import Combine

/// Mesh of each loaded block and it's name.
struct Mesh {
    var name: String
    var mesh: MeshResource
}

/// Indicates whether the player is currently placing or selecting a block.
enum GameState: Int {
    case selectingBlock = 1
    case placingBlock = 2
}


class GameViewController: UIViewController, ARSessionDelegate {
    // MARK: Variables
    
    @IBOutlet var arView: ARView!
    @IBOutlet weak var quitGameButton: UIButton!
    @IBOutlet weak var blockSelectionScrollView: BlockSelectionScrollView!
    @IBOutlet weak var turnBlockLeftButton: UIButton!
    @IBOutlet weak var turnBlockRightButton: UIButton!
    @IBOutlet weak var messageLabel: MessageLabel!
    
    /// Array with all loaded blocks inside.
    var meshes: [Mesh]?
    
    /// Array with all colors, that the blocks can have when changing color through tapping on a block.
    var colors: [SimpleMaterial.Color] = [.blue, .black, .yellow, .green, .cyan, .gray, .magenta, .orange, .brown, .purple]
    
    /// Block that is currently following the camera.
    var followingBlock: FollowingBlock?
    
    /// Session with MultipeerConnectivity.
    var multipeerSession: MultipeerSession?
    
    /// Is true if the player is the host, false if the player is a client in the current game.
    var isHost: Bool = false
    
    var gameState: GameState = .selectingBlock
    
    /// Indicates if the player already placed his first block.
    var firstBlockPlaced = false
    
    /// Indicates if the current game is a multiplayer game.
    var isMultiplayer = true
    
    /// Can be called to play sounds.
    let soundController = SoundController()
    
    /// Indicates if the connection to other players was set up.
    var finishedConnection = false {
        didSet {
            finishSetup()
        }
    }
    
    /// Indiates if ARKit has gathered enough information about the environment.
    var finishedWorldRecognition = false {
        didSet {
            finishSetup()
        }
    }
    
    // MARK: Game Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arView.session.delegate = self
        
        // Load all blocks form the ClassicBlocks.rcproject file and create an array with them.
        meshes = try? ClassicBlocks.loadBlocks().children.first?.children.first?.children.map {e -> Mesh in
            let modelentity = (e.children.first as? ModelEntity)! /*?? (e as? ModelEntity)!*/
            return Mesh(name: e.name, mesh: modelentity.model!.mesh)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Create and set up a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal]
        configuration.isCollaborationEnabled = true
        configuration.environmentTexturing = .automatic // Enable realistic reflections
        
        // Set up the coaching overlay to help the user find planes
        setupCoachingOverlay()
        
        // Set up the block selection scroll view
        setupBlockSelectionView()
        
        // fade out block rotation buttons
        fadeOutRotationButtons()
        
        // Run the view's session
        arView.session.run(configuration)
        
        if isMultiplayer {
            // Set up a multiplayer session and enable block synchronization with RealityKit.
            determineHost()
            multipeerSession = MultipeerSession()
            arView.scene.synchronizationService = multipeerSession?.syncService
        } else {
            self.isHost = true
            self.finishedConnection = true
            self.generateFloor()
        }
        
        // add tap gesture handler that reacts to all taps on the screen. Is used to place blocks or color blocks, depending on the game's state.
        arView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:))))
        
        // ARView options
        arView.renderOptions = [.disableMotionBlur]
        arView.environment.lighting.intensityExponent = 2
        arView.environment.sceneUnderstanding.options.insert(.receivesLighting)
        arView.environment.sceneUnderstanding.options.insert(.physics)
        arView.environment.sceneUnderstanding.options.insert(.collision)
    }
    
    /// Determine if the player wants to be the host of the game, or connect as client. The host-system will placing the blocks and calculating the physics.
    func determineHost() {
        let hostAction = UIAlertAction(title: "Yes", style: .default) { _ in
            // The host-system starts browsing for other peers
            self.isHost = true
            self.multipeerSession?.startBrowsing()
            
            // Guide the user to build a connection with other players.
            self.messageLabel.displayMessage("Please hold your phones next to each other to establish a connection.", duration: 6)
            self.soundController.play(sound: .swish)
            
            // only the host will generate the floor, this leads to a bit better physics performance
            self.generateFloor()
        }
        let guestAction = UIAlertAction(title: "No", style: .default) { _ in
            // The client-system starts advertising himself to other peers.
            self.isHost = false
            self.multipeerSession?.startAdvertising()
            
            // Guide the user to build a connection with other players.
            self.messageLabel.displayMessage("Please hold your phones next to each other to build a connection.", duration: 6)
            self.soundController.play(sound: .swish)
        }
        
        let alertTitle = "Do you want to play as the host?"
        showAlert(title: alertTitle, actions: [hostAction, guestAction])
    }
    
    // MARK: Handling User Inputs
    
    /// Confirm if the user really wants to leave the game, then leave the game.
    @IBAction func quitGamePressed(_ sender: UIButton) {
        // Show an alert to let the user confirm, that he wants to leave the game.
        let leaveAction = UIAlertAction(title: "Leave", style: .cancel) { _ in
            print("Leave game")
            // If the player is the host of the game, remove all anchors from the scene, as they cause problems if a new game is started.
            if (self.isHost) {
                self.removeAllAnchors()
            }
            self.performSegue(withIdentifier: "unwindToMainMenu", sender: self)
        }
        let stayAction = UIAlertAction(title: "Stay", style: .default)
        
        let alertTitle = "Are you sure you want to leave the game?"
        var alertMessage: String?
        
        // Only show this additional message if the user is the host of a multiplayer game, as this removes the ability for other players to continue playing.
        if isHost && isMultiplayer {
            alertMessage = "You’re the host, so if you leave now, the other players will have to leave too."
        }
        showAlert(title: alertTitle, message: alertMessage, actions: [stayAction, leaveAction])
    }
    
    /// Handle the tap of a user anywhere on the screen (except for other UI Elements)
    @objc func handleTap(recognizer: UITapGestureRecognizer) {
        // Check if the user is currently placing a block or not.
        if self.gameState == .placingBlock {
            // disable placement if the block overlaps with existing objects.
            guard  let followingBlock = self.followingBlock, !checkOverlap(followingBlock.model) else { return }
            
            // If this was the first block placed by the user, show a message that explains how to color blocks.
            if(!firstBlockPlaced) {
                messageLabel.displayMessage("To color a block, just tap on it!")
                firstBlockPlaced = true
            }
            
            // Place an anchor for the block 40cm in front of the camera
            placeBlockAnchor(at: moveAway(from: arView.cameraTransform, distance: followingBlock.distance, rotateBy: followingBlock.rotation))
            
            
            // Remove the block following the camera
            removeFollowingBlock()
            
            // Set gameState to selectingBlock
            self.gameState = .selectingBlock
            
            soundController.play(sound: .tapShort)

            //removeProjection()
            
            // fade in block selection menu
            fadeInBlockSelection()
            
            // fade out block rotation buttons
            fadeOutRotationButtons()
        } else {
            // If the user is not placing a block, color any tapped block
            let tapLocation = recognizer.location(in: arView)
            // Get the entity the user touched
            if let entity = arView.entity(at: tapLocation) {
                // Request ownership of the entity, so that the color change will be also synchronized with the other players
                entity.requestOwnership({ [self]_ in
                    // Check if the entity being clicked on is a block
                    for mesh in meshes! {
                        if let anchorEntity = entity.anchor, anchorEntity.name == mesh.name {
                            for child in anchorEntity.children {
                                if let block = child as? ModelEntity {
                                    // Play a splash sound when the block is colored.
                                    soundController.play(sound: .liquidSplash)
                                    
                                    // Change the color of the block.
                                    let previousMaterials = block.model?.materials as? [SimpleMaterial]
                                    let nextMaterials = getNextMaterials(previous: previousMaterials!, all: colors)
                                    block.model?.materials = nextMaterials
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    /// Adds an ARAnchor to the session at the given position. The host of the game will then place a block there.
    func placeBlockAnchor(at transform: Transform){
        if let followingBlock = self.followingBlock {
            // create ARAnchor and add it to the session. The name indicates which block should be placed there.
            let blockAnchor = ARAnchor(name: followingBlock.name, transform: transform.matrix)
            arView.session.add(anchor: blockAnchor)
        }
    }
    
    // MARK: Projection
    
    /// Generates a small sphere that will be displayed at the point where the currently selected block would fall.
    func generateProjection() -> ModelEntity {
        let model = ModelEntity(mesh: .generateSphere(radius: 0.005), materials: [SimpleMaterial(color: .red, isMetallic: true)])
        model.name = "projection"
        
        return model
    }
    
    /// Adjust the position of the sphere that indicates where the currently selected block will fall.
    func adjustProjection() {
        if let followingBlock = self.followingBlock {
            var pos = simd_float3(followingBlock.anchor.position)
            
            // slight offset to fix wrong projection
            pos.y -= 0.05
            
            // Do a raycast downwards and get the first hit object.
            let target = arView.scene.raycast(origin: pos, direction: simd_float3([0, -1, 0])).first(where: {p in p.distance > 0})
            
            if let target = target {
                // Adjust the position of the projection to the result of the raycast.
                followingBlock.anchor.children.first(where: {f in f.name == "projection"})?.position = followingBlock.anchor.convert(position: target.position, from: AnchorEntity(world: [0, 0, 0]))
            }
        }
    }
    
    // MARK: EntityCreation
    
    /// Generates the floor object at a detected horizontal plane.
    func generateFloor(){
        // create an anchor to a detected plane
        let planeAnchor = AnchorEntity(.plane(.horizontal, classification: [.any], minimumBounds: [0.5, 0.5]))
        
        // name entity to detect floor
        planeAnchor.name = "floor"
        
        // create an entity for the floor and give it shape and collision
        let floor = ModelEntity(mesh: .generateBox(size: [1000, 0.01, 1000]), materials: [OcclusionMaterial()])
        floor.generateCollisionShapes(recursive: true)
        
        if let collisionComponent = floor.collision {
            // add a physics body to the floor
            floor.physicsBody = PhysicsBodyComponent(shapes: collisionComponent.shapes, mass: 0, material: .generate(friction: 10, restitution: 0.1), mode: .static)
        }
        
        // add the floor entity to the anchor
        planeAnchor.addChild(floor)
        
        // add the anchor to the scene
        arView.scene.addAnchor(planeAnchor)
    }
    
    /// This will be called if an anchor was added to the scene. The host will add blocks to the session when this is called.
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        // Iterate through all added anchors
        for anchor in anchors {
            if let participantAnchor = anchor as? ARParticipantAnchor {
                // Add a sphere to the location of a player in the session to display which players are connected to your game.
                
                // The participantAnchor will be added automatically, once the connection is completely setup. So this is a good point to indicate the connection setup as done.
                self.finishedConnection = true
                
                // Create an AnchorEntity at the added ParticipantAnchor and add a sphere to it.
                let anchorEntity = AnchorEntity(anchor: participantAnchor)
                
                let coloredSphere = ModelEntity(mesh: MeshResource.generateSphere(radius: 0.05), materials: [SimpleMaterial(color: .white, isMetallic: true)])
                
                anchorEntity.addChild(coloredSphere)
                
                // Add the sphere to the scene.
                arView.scene.addAnchor(anchorEntity)
                
            } else if let anchorName = anchor.name {
                // Check if the player is the host of the game and if the name of the anchor matches a block name.
                if isHost && meshes!.contains(where: {m in
                    m.name == anchorName
                }) {
                    // Only the host will add blocks to the scene to increase the physics performance.
                    
                    // Create an AnchorEntity at the given ARAnchor
                    let anchorEntity = AnchorEntity(anchor: anchor)
                    // Give anchor entity the name of the block it holds
                    anchorEntity.name = anchorName
                    
                    // Get the ModelEntity for the block that should be placed.
                    let modelEntity = getModelEntity(named: anchorName)
                    
                    // Generate collision for the model
                    modelEntity.generateCollisionShapes(recursive: true)
                    
                    // add physics to the block
                    modelEntity.physicsBody = PhysicsBodyComponent(shapes: modelEntity.collision!.shapes, mass: 1000, material: .generate(friction: 10, restitution: 0.1), mode: .dynamic)
                    modelEntity.model?.materials = [SimpleMaterial(color: .blue, isMetallic: false)]
                    modelEntity.synchronization?.ownershipTransferMode = .autoAccept
                    modelEntity.orientation = anchorEntity.orientation
                    
                    // Add the block to the scene
                    anchorEntity.addChild(modelEntity)
                    arView.scene.addAnchor(anchorEntity)
                }
            }
        }
    }
    
    /// Returns a ModelEntity created out of the loaded meshes with the given name.
    func getModelEntity(named: String) -> ModelEntity {
        ModelEntity(mesh: (meshes?.first(where: {m in m.name == named}))!.mesh)
    }
    
    /// Check whether the given ModelEntity intersects with other objects in the scene
    func checkOverlap(_ block: ModelEntity) -> Bool {
        for anchor in arView.scene.anchors {
            // Check if the given ModelEntity intersects with any other block
            for mesh in meshes! {
                if let blockEntity = anchor.findEntity(named: mesh.name) {
                    // If the given ModelEntity intersects with another block return true
                    if blockEntity.visualBounds(relativeTo: nil).intersects(block.visualBounds(relativeTo: nil)) {
                        return true
                    }
                }
            }
            // Check if the given ModelEntity intersects with the floor, if yes return true.
            if let floorEntity = anchor.findEntity(named: "floor") {
                if floorEntity.visualBounds(relativeTo: nil).intersects(block.visualBounds(relativeTo: nil)) {
                    return true
                }
            }
        }
        // If we reach this, the given ModelEntity did not intersect with anything, so we return false.
        return false
    }
    
    /// Returns a material array for a block, where the first material is the main one and the remaining ones are used for determining the next color
    func getNextMaterials(previous materials: [SimpleMaterial], all colors: [SimpleMaterial.Color]) -> [SimpleMaterial] {
        var nextMaterials: [SimpleMaterial]
        let previousMaterialCount = materials.count
        if previousMaterialCount ==  colors.count { // Start the color iteration from scratch
            nextMaterials = [SimpleMaterial(color: .blue, isMetallic: false)]
        } else { // Iterate through the colors
            nextMaterials = [SimpleMaterial(color: colors[previousMaterialCount], isMetallic: false)] // Set the first color
            nextMaterials.append(contentsOf: materials)
        }
        return nextMaterials
    }
    
    /// Returns a Transform that is moved away by the given distance and rotated around the z-axis (from the players point of view) by the given degrees.
    func moveAway(from transform: Transform, distance: Float, rotateBy degrees: Int) -> Transform {
        // create matrix that moves position away by the given distance
        let translate = float4x4(
            [1,0,0,0],
            [0,1,0,0],
            [0,0,1,0],
            [0,0,-distance,1]
        )
        
        // Apply position adjustments
        var newTransform = Transform(matrix: transform.matrix * translate)
        
        // Rotate block around the z- axis by the given degrees
        let a = Double(degrees) * Double.pi / 180
        
        // Rotation matrix for rotation around the z-axis.
        let translateRotation = float4x4(
            [Float(cos(a)),Float(-sin(a)),0,0],
            [Float(sin(a)),Float(cos(a)),0,0],
            [0,0,1,0],
            [0,0,0,1]
        )
        
        // Set rotation on x- and z- axis to 0
        var rotation = transform.rotation.vector
        rotation.x = 0
        rotation.z = 0
        newTransform.rotation = simd_quatf(vector: rotation)
        
        // Apply the rotation.
        newTransform.matrix = newTransform.matrix * translateRotation
        
        return newTransform
    }
    
    /// Shows  a message to the player if the setup is done
    func finishSetup(){
        guard self.finishedConnection && self.finishedWorldRecognition else { return }
        
        self.messageLabel.displayMessage("Setup done! Select a block and tap anywhere to drop it.", duration: 6)
        soundController.play(sound: .successTone)
    }
    
    /// Removes all anchors from the scene.
    func removeAllAnchors() {
        for anchor in arView.scene.anchors {
            anchor.removeFromParent()
        }
    }
}


extension UIViewController {
    /// Show an alert message with the given title, message and the defined actions.
    func showAlert(title: String, message: String? = nil, actions: [UIAlertAction]? = nil) {
        // Create an AlertController with the given message and title.
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Add all given actions to the alertController
        if let actions = actions {
            actions.forEach { alertController.addAction($0) }
        } else {
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
        
        // Present the alertController.
        present(alertController, animated: true, completion: nil)
    }
    
    /// This is needed, otherwise returning to the home screen does not work.
    @IBAction func unwind( _ seg: UIStoryboardSegue) {
    }
}
