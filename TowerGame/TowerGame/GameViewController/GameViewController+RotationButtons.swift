//
//  GameViewController+RotationButtons.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import SwiftUI


extension GameViewController {
    
    /// Rotates the selected block by 90 degrees counter-clock-wise.
    @IBAction func turnBlockLeftPressed(_ sender: Any) {
        print("turn left button pressed")
        if let followingBlock = self.followingBlock {
            self.followingBlock?.rotation = (followingBlock.rotation - 90) % 360
        }
    }
    
    /// Rotates the selected block by 90 degrees clock-wise.
    @IBAction func turnBlockRightPressed(_ sender: Any) {
        print("turn right button pressed")
        if let followingBlock = self.followingBlock {
            self.followingBlock?.rotation = (followingBlock.rotation + 90) % 360
        }
    }
    
    /// Fade in the rotation buttons to make them fully visible when placing a block.
    func fadeInRotationButtons() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.turnBlockRightButton.alpha = 1.0
            self.turnBlockLeftButton.alpha = 1.0
        }, completion: {(finished: Bool) -> Void in })
        
        // Enable the buttons to rotate the selected block.
        self.turnBlockRightButton.isEnabled = true
        self.turnBlockLeftButton.isEnabled = true
    }
    
    /// Fade out the rotation buttons to make them transparent when not placing a block.
    func fadeOutRotationButtons() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.turnBlockRightButton.alpha = 0.3
            self.turnBlockLeftButton.alpha = 0.3
        }, completion: {(finished: Bool) -> Void in })
        
        // Disable the buttons to rotate the selected block.
        self.turnBlockRightButton.isEnabled = false
        self.turnBlockLeftButton.isEnabled = false
    }
}
