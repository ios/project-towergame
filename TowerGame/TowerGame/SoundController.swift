//
//  SoundController.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import AVKit

/// Controlls the currently played sounds.
class SoundController: NSObject, AVAudioPlayerDelegate {
    /// Set of sounds that are currently played to allow playing multiple sounds at once.
    private var currentlyPlaying = Set<AVAudioPlayer>()

    /// Plays the given [sound](x-source-tag://Sound).
    func play(sound: Sound) {
        do {
            // Get the URL to the given sound file.
            if let urlString = Bundle.main.path(forResource: sound.rawValue, ofType: nil){
                // Create a new AVAudioPlayer, set self as it's delegate and play the sound.
                let audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: urlString))
                audioPlayer.delegate = self
                currentlyPlaying.insert(audioPlayer)
                audioPlayer.play()
                print("played \(sound)")
            }
        } catch {
            print("File could not be loaded")
        }
    }
    
    /// Implementation of the AVAudioPlayer's delegate method, that gets called once the sound finished playing. Removes the player from the set of currently playing players.
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("finished")
        currentlyPlaying.remove(player)
    }
    
}

/// - Tag: Sound
/// Enum that contains the paths to all sound files available in this app. Makes the handling of sounds easier and reduces the risk of trying to access non-existent filess.
enum Sound: String {
    case liquidSplash = "Liquid-Splash-Small-01.m4a"
    case mouthPop = "Mouth-Pop-Short.wav"
    case popHigh = "Pop-High-Round-Short-01.wav"
    case synthPop = "Synth-Pop-Small-01.m4a"
    case tapShort = "Tap-Short-Reverse-01.wav"
    case warmTone = "Warm-Tone-06.wav"
    case swish = "Swish-Swish-01.wav"
    case successTone = "Warm-Tone-01.wav"
}
