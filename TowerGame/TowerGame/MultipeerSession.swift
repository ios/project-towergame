//
//  MultipeerSession.swift
//  TowerGame
//
//  Created by by Group H in winter semester 2020-2021.
//

import MultipeerConnectivity
import RealityKit

/// This class is an adjusted version of the one apple used in their sample code "Creating a Collaborative Session" ( https://developer.apple.com/documentation/arkit/creating_a_collaborative_session ).
class MultipeerSession: NSObject {
    /// Identifier of the serviceType, so that the session knows for which other sessions it should look.
    static let serviceType = "grph-towergame"
    
    /// ID of this peer, with the device's name as displayName.
    private let myPeerID = MCPeerID(displayName: UIDevice.current.name)
    
    /// Used to advertise this peer to other peers.
    private var serviceAdvertiser: MCNearbyServiceAdvertiser!
    
    /// Used to look for other peers nearby.
    private var serviceBrowser: MCNearbyServiceBrowser!
    private var session: MCSession!
    
    /// Service that synchronizes the scenes among all peers. Is needed for RealityKit's synchronization.
    public var syncService: MultipeerConnectivityService? {
      return try? MultipeerConnectivityService(session: session)
    }
    
    /// List of all connectedPeers
    var connectedPeers: [MCPeerID] {
        return session.connectedPeers
    }

    /// Constructor that creates a new session.
    override init() {
        super.init()
        
        session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .required)
    }
    
    /// Function to start advertising the peer to other peers. Is called by the host peer.
    func startAdvertising() {
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: nil, serviceType: MultipeerSession.serviceType)
        serviceAdvertiser.delegate = self
        serviceAdvertiser.startAdvertisingPeer()
        
    }
    
    /// Function to start browsing for other nearby peers. Is called by the client peer.
    func startBrowsing() {
        serviceBrowser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: MultipeerSession.serviceType)
        serviceBrowser.delegate = self
        serviceBrowser.startBrowsingForPeers()
    }
    
    /// Function that is called when a peer was discovered and returns if it should be invited to join the session.
    func peerDiscovered(_ peer: MCPeerID) -> Bool {
        if self.connectedPeers.count > 3 {
            // We only want to allow 4 connections at the same time, this seems to be a common value. As we could not test it with more than 2 we just accepted this.
            print("Can't join, only 4 connections possible.")
            return false
        } else {
            return true
        }
    }
    
}

extension MultipeerSession: MCNearbyServiceBrowserDelegate {
    
    /// Implementation of the browser's delegate function that is called once a peer was discovered nearby.
    public func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String: String]?) {
        // Check if more connections are allowed.
        let accepted = peerDiscovered(peerID)
        if accepted {
            // Send invite to the discovered peer.
            browser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
        }
    }

    public func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        // This app doesn't do anything with non-invited peers, so there's nothing to do here.
    }
    
}

extension MultipeerSession: MCNearbyServiceAdvertiserDelegate {
    
    /// Implementation of the advertiser's delegate function that is called once the peer received an invitation. Always accepts the invitation, because the host already checked if the connection should be build.
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID,
                    withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        // Accept the peer's invitation
        invitationHandler(true, self.session)
    }
}
